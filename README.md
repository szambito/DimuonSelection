*) Setup the release & Panda:
-----------------------------
asetup 20.7.5.1,here
lsetup panda
voms-proxy-init -voms atlas


*) Compile
-----------------------------
cmt find_packages
cmt compile


*) To run locally:
-----------------------------
* 1) After compilation, edit 'DataPath' inside 'InstallArea/jobOptions/AthMuonSelector/AthMuonSelector_JobOptions.py'
     specifying the folder where your input (D)xAODs are stored; in the same file you can set the output level, the 
     maximum number of events you want to be processed, and so on...
* 2) Simply, 'athena InstallArea/jobOptions/AthMuonSelector/AthMuonSelector_JobOptions.py' 


*) To run on the GRID:
-----------------------------
* 1) Edit 'DataSets' inside 'InstallArea/jobOptions/AthMuonSelector/AthMuonSelector_SendToGrid.py', adding a comma-separated
     list of the GRID datasets you want to run over
* 2) Edit 'UserName' and 'OutputDataSet' according to your specific needs
* 3) Simply, 'python InstallArea/jobOptions/AthMuonSelector/AthMuonSelector_SendToGrid.py' 