# Set up the file reading:
from glob import glob
import AthenaPoolCnvSvc.ReadAthenaPool
DataPath = '/tmp/szambito/data15_13TeV.00284285.physics_Main.merge.DAOD_MUON0.r7562_p2521_p2510_tid07706258_00/'
ServiceMgr.EventSelector.InputCollections = glob( DataPath + '*.root*' ) 

# Setup the global algorithm
from AthenaCommon.AlgSequence import AlgSequence
theJob = AlgSequence()

# THistSvc configuration
from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output += [ "MCPVALID DATAFILE='MuonDataMcValidation_xAOD.root' OPT='RECREATE'" ]  

# Setup GRL
GRL = os.path.expandvars( '$TestArea/AthMuonSelector/grl/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml' )
ToolSvc += CfgMgr.GoodRunsListSelectionTool( 'GRLTool' , GoodRunsListVec = [ GRL ] )

# Setup MST
ToolSvc += CfgMgr.CP__MuonSelectionTool( 'MuonSelectorTool', OutputLevel = DEBUG )

# MomentumCorrections
ToolSvc += CfgMgr.CP__MuonCalibrationAndSmearingTool( 'MuonCorrectionTool', OutputLevel = INFO )

# Isolation
ToolSvc += CfgMgr.CP__IsolationSelectionTool( "IsolationSelectionTool", MuonWP = "Tight", OutputLevel = INFO )

from AthMuonSelector.AthMuonSelectorConf import AthMuonSelectorTool
SelectorTool = AthMuonSelectorTool( 'SelectorTool', OutputLevel = INFO ) 
SelectorTool.MuonSelectorTool = ToolSvc.MuonSelectorTool
ToolSvc += SelectorTool

# Setup algorithm
from AthMuonSelector.AthMuonSelectorConf import AthMuonNtupleMaker
NtupleMaker = AthMuonNtupleMaker( 'NtupleMaker', OutputLevel = INFO )
NtupleMaker.SelectorTool = SelectorTool
NtupleMaker.ExtendedVersion = False
theJob += NtupleMaker

# Do some additional tweaking:
from AthenaCommon.AppMgr import theApp
theApp.EvtMax = -1
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 10000

# MAx number of events
#theApp.EvtMax = 10000
