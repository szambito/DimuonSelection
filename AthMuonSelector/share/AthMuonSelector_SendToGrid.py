import os, re, sys

#=#=#
DataSets = [
  'data15_13TeV.00284484.physics_Main.merge.DAOD_MUON0.r7562_p2521_p2510',
  'data15_13TeV.00284484.physics_Main.merge.DAOD_MUON0.r7562_p2521_p2510',
]

#=#=#
PrintOnly = False
IsOfficial = False
Version = 'v0'
FilesPerJob = 20
TotalFiles = -1

#=#=#
if IsOfficial:
  UserName = 'group.perf-muons'
else:
  UserName = 'user.szambito'

#=#=#
RegEx_MC = re.compile( '(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)/' ) 
RegEx_Data = re.compile( '(\w*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)/' ) 
JobOptions = os.path.expandvars( '$TestArea/InstallArea/jobOptions/AthMuonSelector/AthMuonSelector_JobOptions.py' )

for DataSet in DataSets:
  Match_MC = RegEx_MC.match( DataSet ) 
  Match_Data = RegEx_Data.match( DataSet ) 
  OutputDataSet = UserName + '.' + DataSet + '.ValidNtp.' + Version
  if Match_MC:
    OutputDataSet = UserName + '.' + Match_MC.group( 1 ) + '.' + Match_MC.group( 2 ) + '.' + Match_MC.group( 3 ) + '.ValidNtp.' + Version
  elif Match_Data:
    OutputDataSet = UserName + '.' + Match_Data.group( 1 ) + '.' + Match_Data.group( 2 ) + '.' + Match_Data.group( 3 ) + '.' + Match_Data.group( 6 ) + '.ValidNtp.' + Version
  Command  = 'pathena'
  Command += ' %s' % JobOptions
  Command += ' --inDS=%s' % (DataSet +'/')
  Command += ' --outDS=%s' % OutputDataSet
  Command += ' --nFilesPerJob=%d' % FilesPerJob 
  if TotalFiles > 0:
    Command += ' --nFiles=%d' % TotalFiles
  if IsOfficial:
    Command += ' --official --voms=atlas:/atlas/perf-muons/Role=production' 
  Command += ' --skipScout'
  Command += ' --noEmail'

  if PrintOnly:
    print Command
  else:
    os.system( Command )
