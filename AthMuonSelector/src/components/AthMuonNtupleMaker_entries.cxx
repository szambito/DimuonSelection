#include "GaudiKernel/DeclareFactoryEntries.h"

#include "AthMuonSelector/AthMuonNtupleMaker.h"
#include "AthMuonSelector/AthMuonSelectorTool.h"


DECLARE_ALGORITHM_FACTORY( AthMuonNtupleMaker )
DECLARE_TOOL_FACTORY( AthMuonSelectorTool )

