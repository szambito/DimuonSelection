//::: STL includes
#include <string>

//::: Local includes
#include "AthMuonSelector/AthMuonNtupleMaker.h"

AthMuonNtupleMaker::AthMuonNtupleMaker( const std::string& name, ISvcLocator* pSvcLocator ) : 
  ::AthAlgorithm( name, pSvcLocator ),
  m_SelectorTool( "CalibMuonsSelTool", this ),
  m_HistSvc( "THistSvc", name ),
  m_CutFlow(NULL), m_OutputTree(NULL),
  m_RunNumber(0), m_LumiBlock(0), m_ChannelNumber(0), m_averageInteractionsPerCrossing(0),
  m_EvtNumber(0), m_EventWeight(-99.),
  m_Lead_z0_exBS(-9999999999.99), m_Sub_z0_exBS(-9999999999.99), 
  m_Lead_z0_exPV(-9999999999.99), m_Sub_z0_exPV(-9999999999.99), 
  m_Lead_d0_exBS(-9999999999.99), m_Sub_d0_exBS(-9999999999.99), 
  m_Lead_Pt(0.), m_Sub_Pt(0.),
  m_Lead_Eta(0.), m_Sub_Eta(0.),
  m_Lead_Phi(0.), m_Sub_Phi(0.),
  m_Lead_qOverP(0.), m_Sub_qOverP(0.),
  m_Lead_qOverPerr(0.), m_Sub_qOverPerr(0.),
  m_Lead_Charge(0.), m_Sub_Charge(0.),
  m_Lead_ID_Pt(0.), m_Sub_ID_Pt(0.),
  m_Lead_ID_Eta(0.), m_Sub_ID_Eta(0.),
  m_Lead_ID_Phi(0.), m_Sub_ID_Phi(0.),
  m_Lead_ID_qOverP(0.), m_Sub_ID_qOverP(0.),
  m_Lead_ID_qOverPerr(0.), m_Sub_ID_qOverPerr(0.),
  m_Lead_ME_Pt(0.), m_Sub_ME_Pt(0.),
  m_Lead_ME_Eta(0.), m_Sub_ME_Eta(0.),
  m_Lead_ME_Phi(0.), m_Sub_ME_Phi(0.),
  m_Lead_ME_qOverP(0.), m_Sub_ME_qOverP(0.),
  m_Lead_ME_qOverPerr(0.), m_Sub_ME_qOverPerr(0.),
  m_Lead_MS_Pt(0.), m_Sub_MS_Pt(0.),
  m_Lead_MS_Eta(0.), m_Sub_MS_Eta(0.),
  m_Lead_MS_Phi(0.), m_Sub_MS_Phi(0.),
  m_Lead_MS_qOverP(0.), m_Sub_MS_qOverP(0.),
  m_Lead_MS_qOverPerr(0.), m_Sub_MS_qOverPerr(0.),
  m_Lead_PtCone20(0.), m_Sub_PtCone20(0.),
  m_Lead_Quality(0), m_Sub_Quality(0),
  m_Lead_Author(0), m_Sub_Author(0),
  m_Lead_Type(0), m_Sub_Type(0),
  m_Lead_EnergyLoss(0), m_Sub_EnergyLoss(0),
  m_Lead_EnergyLoss_Err(0), m_Sub_EnergyLoss_Err(0),
  m_Lead_EnergyLoss_Meas(0), m_Sub_EnergyLoss_Meas(0),
  m_Lead_EnergyLoss_Par(0), m_Sub_EnergyLoss_Par(0),
  m_Lead_EnergyLossType(0), m_Sub_EnergyLossType(0),
  m_Lead_PrimarySector(0), m_Sub_PrimarySector(0),
  m_Lead_SecondarySector(0), m_Sub_SecondarySector(0),
  m_CB_Mass(0.), m_CB_Mass_Smeared(0.), 
  m_ID_Mass(0.), m_ID_Mass_Smeared(0.),
  m_ME_Mass(0.), m_ME_Mass_Smeared(0.),
  m_CB_Pt(0.), m_ID_Pt(0.), m_ME_Pt(0.),
  m_CB_Eta(0.), m_ID_Eta(0.), m_ME_Eta(0.),
  m_CB_Phi(0.), m_ID_Phi(0.), m_ME_Phi(0.),
  m_Lead_Pt_Smeared(0.), m_Sub_Pt_Smeared(0.),
  m_Lead_ID_Pt_Smeared(0.), m_Sub_ID_Pt_Smeared(0.),
  m_Lead_ME_Pt_Smeared(0.), m_Sub_ME_Pt_Smeared(0.),
  // ::
  m_Lead_nPrecLayers (0), m_Sub_nPrecLayers(0),
  m_Lead_nGoodPrecLayers (0), m_Sub_nGoodPrecLayers(0),
  m_Lead_nPrecHoles (0), m_Sub_nPrecHoles(0),
  m_Lead_nPhiLayers (0), m_Sub_nPhiLayers(0),
  m_Lead_nPhiHoleLayers (0), m_Sub_nPhiHoleLayers(0),
  m_Lead_nTriggerEtaLayers (0), m_Sub_nTriggerEtaLayers(0),
  m_Lead_nTriggerEtaHoleLayers (0), m_Sub_nTriggerEtaHoleLayers(0),
  // :: 
  m_Lead_ReducedChi2(0.), m_Sub_ReducedChi2(0.),
  m_Lead_ID_ReducedChi2(0.), m_Sub_ID_ReducedChi2(0.),
  m_Lead_passedIDCuts(0), m_Sub_passedIDCuts(0)
{

  declareProperty( "SelectorTool", m_SelectorTool );
  declareProperty( "ExtendedVersion", m_Extended = false );
 
}

StatusCode AthMuonNtupleMaker::initialize() {

  ATH_MSG_INFO( "Initializing " << name() << "..." );

  StatusCode sc = m_SelectorTool.retrieve();
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to retrieve AlgTool " << m_SelectorTool );
    return sc;
  }

  sc = m_HistSvc.retrieve();
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to retrieve HistSvc" );
    return sc;
  }

  m_OutputTree = new TTree( "DimuonsTree", "" );

  m_OutputTree->Branch( "RunNumber",     &m_RunNumber     );
  m_OutputTree->Branch( "EvtNumber",     &m_EvtNumber     );
  m_OutputTree->Branch( "averageIntPerXing",     &m_averageInteractionsPerCrossing );
  m_OutputTree->Branch( "LumiBlock",     &m_LumiBlock     );
  m_OutputTree->Branch( "ChannelNumber", &m_ChannelNumber );
  m_OutputTree->Branch( "EventWeight",   &m_EventWeight   );
  // ::
  m_OutputTree->Branch( "Lead_z0_exPV",        &m_Lead_z0_exPV         );
  m_OutputTree->Branch( "Lead_z0_exBS",        &m_Lead_z0_exBS         );
  m_OutputTree->Branch( "Lead_d0_exBS",        &m_Lead_d0_exBS         );
  m_OutputTree->Branch( "Lead_d0_exBSerr",     &m_Lead_d0_exBSerr      );
  m_OutputTree->Branch( "Sub_z0_exPV",         &m_Sub_z0_exPV          );
  m_OutputTree->Branch( "Sub_z0_exBS",         &m_Sub_z0_exBS          );
  m_OutputTree->Branch( "Sub_d0_exBS",         &m_Sub_d0_exBS          );
  m_OutputTree->Branch( "Sub_d0_exBSerr",      &m_Sub_d0_exBSerr       );
  m_OutputTree->Branch( "Lead_Pt",              &m_Lead_Pt              );
  m_OutputTree->Branch( "Lead_Pt_Smeared",      &m_Lead_Pt_Smeared      );
  m_OutputTree->Branch( "Lead_Eta",             &m_Lead_Eta             );
  m_OutputTree->Branch( "Lead_Phi",             &m_Lead_Phi             );
  m_OutputTree->Branch( "Lead_qOverP",          &m_Lead_qOverP          );
  m_OutputTree->Branch( "Lead_qOverPerr",       &m_Lead_qOverPerr       );
  m_OutputTree->Branch( "Lead_Charge",          &m_Lead_Charge          );
  m_OutputTree->Branch( "Lead_ID_Pt",           &m_Lead_ID_Pt           );
  m_OutputTree->Branch( "Lead_ID_Pt_Smeared",   &m_Lead_ID_Pt_Smeared   );
  m_OutputTree->Branch( "Lead_ID_Eta",          &m_Lead_ID_Eta          );
  m_OutputTree->Branch( "Lead_ID_Phi",          &m_Lead_ID_Phi          );
  m_OutputTree->Branch( "Lead_ID_qOverP",       &m_Lead_ID_qOverP       );
  m_OutputTree->Branch( "Lead_ID_qOverPerr",    &m_Lead_ID_qOverPerr    );
  m_OutputTree->Branch( "Lead_ME_Pt",           &m_Lead_ME_Pt           );
  m_OutputTree->Branch( "Lead_ME_Pt_Smeared",   &m_Lead_ME_Pt_Smeared   );
  m_OutputTree->Branch( "Lead_ME_Eta",          &m_Lead_ME_Eta          );
  m_OutputTree->Branch( "Lead_ME_Phi",          &m_Lead_ME_Phi          );
  m_OutputTree->Branch( "Lead_ME_qOverP",       &m_Lead_ME_qOverP       );
  m_OutputTree->Branch( "Lead_ME_qOverPerr",    &m_Lead_ME_qOverPerr    );
  m_OutputTree->Branch( "Lead_MS_Pt",           &m_Lead_MS_Pt           );
  m_OutputTree->Branch( "Lead_MS_Eta",          &m_Lead_MS_Eta          );
  m_OutputTree->Branch( "Lead_MS_Phi",          &m_Lead_MS_Phi          );
  m_OutputTree->Branch( "Lead_MS_qOverP",       &m_Lead_MS_qOverP       );
  m_OutputTree->Branch( "Lead_MS_qOverPerr",    &m_Lead_MS_qOverPerr    );
  m_OutputTree->Branch( "Lead_PtVarCone20",     &m_Lead_PtCone20        );
  m_OutputTree->Branch( "Lead_PtVarCone30",     &m_Lead_PtCone30        );
  m_OutputTree->Branch( "Lead_TopoEtCone20",    &m_Lead_TopoEtCone20    );
  m_OutputTree->Branch( "Lead_TopoEtCone30",    &m_Lead_TopoEtCone30    );
  m_OutputTree->Branch( "Lead_Quality",         &m_Lead_Quality         );
  m_OutputTree->Branch( "Lead_passedIDCuts",    &m_Lead_passedIDCuts    );
  m_OutputTree->Branch( "Lead_Author",          &m_Lead_Author          );
  m_OutputTree->Branch( "Lead_Type",            &m_Lead_Type            );
  m_OutputTree->Branch( "Lead_EnergyLoss",      &m_Lead_EnergyLoss      ); 
  m_OutputTree->Branch( "Lead_EnergyLoss_Meas",    &m_Lead_EnergyLoss_Meas    );
  m_OutputTree->Branch( "Lead_EnergyLoss_Par",      &m_Lead_EnergyLoss_Par     );
  m_OutputTree->Branch( "Lead_EnergyLossSigma",    &m_Lead_EnergyLoss_Err      );
  m_OutputTree->Branch( "Lead_EnergyLossType",  &m_Lead_EnergyLossType  );
  m_OutputTree->Branch( "Lead_PrimarySector",   &m_Lead_PrimarySector   );
  m_OutputTree->Branch( "Lead_SecondarySector", &m_Lead_SecondarySector );
  m_OutputTree->Branch( "Lead_nPrecLayers", &m_Lead_nPrecLayers );
  m_OutputTree->Branch( "Lead_nGoodPrecLayers", &m_Lead_nGoodPrecLayers );
  m_OutputTree->Branch( "Lead_nPrecHoles", &m_Lead_nPrecHoles );
  m_OutputTree->Branch( "Lead_nPhiLayers", &m_Lead_nPhiLayers );
  m_OutputTree->Branch( "Lead_nPhiHoleLayers", &m_Lead_nPhiHoleLayers );
  m_OutputTree->Branch( "Lead_nTriggerEtaLayers", &m_Lead_nTriggerEtaLayers );
  m_OutputTree->Branch( "Lead_nTriggerEtaHoleLayers", &m_Lead_nTriggerEtaHoleLayers );
  m_OutputTree->Branch( "Lead_ReducedChi2", &m_Lead_ReducedChi2 );
  m_OutputTree->Branch( "Lead_ID_ReducedChi2", &m_Lead_ID_ReducedChi2 );
  // ::
  m_OutputTree->Branch( "Sub_Pt",              &m_Sub_Pt              );
  m_OutputTree->Branch( "Sub_Pt_Smeared",      &m_Sub_Pt_Smeared      );
  m_OutputTree->Branch( "Sub_Eta",             &m_Sub_Eta             );
  m_OutputTree->Branch( "Sub_Phi",             &m_Sub_Phi             );
  m_OutputTree->Branch( "Sub_qOverP",          &m_Sub_qOverP          );
  m_OutputTree->Branch( "Sub_qOverPerr",       &m_Sub_qOverPerr       );
  m_OutputTree->Branch( "Sub_Charge",          &m_Sub_Charge          );
  m_OutputTree->Branch( "Sub_ID_Pt",           &m_Sub_ID_Pt           );
  m_OutputTree->Branch( "Sub_ID_Pt_Smeared",   &m_Sub_ID_Pt_Smeared   );
  m_OutputTree->Branch( "Sub_ID_Eta",          &m_Sub_ID_Eta          );
  m_OutputTree->Branch( "Sub_ID_Phi",          &m_Sub_ID_Phi          );
  m_OutputTree->Branch( "Sub_ID_qOverP",       &m_Sub_ID_qOverP       );
  m_OutputTree->Branch( "Sub_ID_qOverPerr",    &m_Sub_ID_qOverPerr    );
  m_OutputTree->Branch( "Sub_ME_Pt",           &m_Sub_ME_Pt           );
  m_OutputTree->Branch( "Sub_ME_Pt_Smeared",   &m_Sub_ME_Pt_Smeared   );
  m_OutputTree->Branch( "Sub_ME_Eta",          &m_Sub_ME_Eta          );
  m_OutputTree->Branch( "Sub_ME_Phi",          &m_Sub_ME_Phi          );
  m_OutputTree->Branch( "Sub_ME_qOverP",       &m_Sub_ME_qOverP       );
  m_OutputTree->Branch( "Sub_ME_qOverPerr",    &m_Sub_ME_qOverPerr    );
  m_OutputTree->Branch( "Sub_MS_Pt",           &m_Sub_MS_Pt           );
  m_OutputTree->Branch( "Sub_MS_Eta",          &m_Sub_MS_Eta          );
  m_OutputTree->Branch( "Sub_MS_Phi",          &m_Sub_MS_Phi          );
  m_OutputTree->Branch( "Sub_MS_qOverP",       &m_Sub_MS_qOverP       );
  m_OutputTree->Branch( "Sub_MS_qOverPerr",    &m_Sub_MS_qOverPerr    );
  m_OutputTree->Branch( "Sub_PtVarCone20",     &m_Sub_PtCone20        );
  m_OutputTree->Branch( "Sub_PtVarCone30",     &m_Sub_PtCone30        );
  m_OutputTree->Branch( "Sub_TopoEtCone20",    &m_Sub_TopoEtCone20    );
  m_OutputTree->Branch( "Sub_TopoEtCone30",    &m_Sub_TopoEtCone30    );
  m_OutputTree->Branch( "Sub_Quality",         &m_Sub_Quality         );
  m_OutputTree->Branch( "Sub_passedIDCuts",    &m_Sub_passedIDCuts    );
  m_OutputTree->Branch( "Sub_Author",          &m_Sub_Author          );
  m_OutputTree->Branch( "Sub_Type",            &m_Sub_Type            );
  m_OutputTree->Branch( "Sub_EnergyLoss",      &m_Sub_EnergyLoss      );
  m_OutputTree->Branch( "Sub_EnergyLossType",  &m_Sub_EnergyLossType  );
  m_OutputTree->Branch( "Sub_EnergyLoss_Meas",    &m_Sub_EnergyLoss_Meas    );
  m_OutputTree->Branch( "Sub_EnergyLoss_Par",      &m_Sub_EnergyLoss_Par     );
  m_OutputTree->Branch( "Sub_EnergyLossSigma",      &m_Sub_EnergyLoss_Err      );
  m_OutputTree->Branch( "Sub_PrimarySector",   &m_Sub_PrimarySector   );
  m_OutputTree->Branch( "Sub_SecondarySector", &m_Sub_SecondarySector );
  m_OutputTree->Branch( "Sub_nPrecLayers", &m_Sub_nPrecLayers );
  m_OutputTree->Branch( "Sub_nGoodPrecLayers", &m_Sub_nGoodPrecLayers );
  m_OutputTree->Branch( "Sub_nPrecHoles", &m_Sub_nPrecHoles );
  m_OutputTree->Branch( "Sub_nPhiLayers", &m_Sub_nPhiLayers );
  m_OutputTree->Branch( "Sub_nPhiHoleLayers", &m_Sub_nPhiHoleLayers );
  m_OutputTree->Branch( "Sub_nTriggerEtaLayers", &m_Sub_nTriggerEtaLayers );
  m_OutputTree->Branch( "Sub_nTriggerEtaHoleLayers", &m_Sub_nTriggerEtaHoleLayers );
  m_OutputTree->Branch( "Sub_ReducedChi2", &m_Sub_ReducedChi2 );
  m_OutputTree->Branch( "Sub_ID_ReducedChi2", &m_Sub_ID_ReducedChi2 );
  // ::
  m_OutputTree->Branch( "Dimuon_CB_Mass", &m_CB_Mass );
  m_OutputTree->Branch( "Dimuon_CB_Mass_Smeared", &m_CB_Mass_Smeared );
  m_OutputTree->Branch( "Dimuon_ID_Mass", &m_ID_Mass );
  m_OutputTree->Branch( "Dimuon_ID_Mass_Smeared", &m_ID_Mass_Smeared );
  m_OutputTree->Branch( "Dimuon_ME_Mass", &m_ME_Mass );
  m_OutputTree->Branch( "Dimuon_ME_Mass_Smeared", &m_ME_Mass_Smeared );
  m_OutputTree->Branch( "Dimuon_CB_Pt",   &m_CB_Pt   );
  m_OutputTree->Branch( "Dimuon_ID_Pt",   &m_ID_Pt   );
  m_OutputTree->Branch( "Dimuon_ME_Pt",   &m_ME_Pt   );
  m_OutputTree->Branch( "Dimuon_CB_Eta",  &m_CB_Eta  );
  m_OutputTree->Branch( "Dimuon_ID_Eta",  &m_ID_Eta  );
  m_OutputTree->Branch( "Dimuon_ME_Eta",  &m_ME_Eta  );
  m_OutputTree->Branch( "Dimuon_CB_Phi",  &m_CB_Phi  );
  m_OutputTree->Branch( "Dimuon_ID_Phi",  &m_ID_Phi  );
  m_OutputTree->Branch( "Dimuon_ME_Phi",  &m_ME_Phi  );

  sc = m_HistSvc->regTree( "/MCPVALID/DimuonsTree", m_OutputTree );
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to register output TTree" );
    return sc;
  }

  m_CutFlow = new TH1F( "CutFlow", "", 20, 0, 20 );

  sc = m_HistSvc->regHist( "/MCPVALID/CutFlow", m_CutFlow );
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to register cut flow histogram" );
    return sc;
  }

  return StatusCode::SUCCESS;

}

StatusCode AthMuonNtupleMaker::finalize() {

  ATH_MSG_INFO( "Finalizing " << name() << "..." );
  return StatusCode::SUCCESS;

}

StatusCode AthMuonNtupleMaker::execute() {  

  ATH_MSG_DEBUG( "Executing " << name() << "..." );

  const xAOD::MuonContainer* inputMuons = 0;
  StatusCode sc = evtStore()->retrieve( inputMuons, "Muons" );
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to retrieve MuonContainer with key: Muons" );
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG( "MuonContainer: " << inputMuons );

  PairsVector outputMuons = m_SelectorTool->GetMuons( inputMuons, m_CutFlow );

  ATH_MSG_DEBUG( "In " << name() << ", vector of pairs size: " << outputMuons.size() );
 
  const xAOD::EventInfo* evtInfo = 0;
  sc = evtStore()->retrieve( evtInfo, "EventInfo" );
  if( sc.isFailure() ) {
    ATH_MSG_ERROR( "Failed to retrieve EventInfo object" );
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG( "EventInfo: " << evtInfo );

  m_RunNumber = evtInfo->runNumber();
  m_EvtNumber = evtInfo->eventNumber();
  m_LumiBlock = evtInfo->lumiBlock();
  m_ChannelNumber = ( evtInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? evtInfo->mcChannelNumber() : 0;
  m_EventWeight = ( evtInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? evtInfo->mcEventWeight() : 1.;
  m_averageInteractionsPerCrossing = evtInfo->averageInteractionsPerCrossing();

  const xAOD::VertexContainer* vertices(0);
  xAOD::Vertex* vtx(0);
  if( evtStore()->retrieve( vertices, "PrimaryVertices" ).isSuccess() ) {
    for( const auto& vx : *vertices ) {
      if(vx->vertexType() == xAOD::VxType::PriVtx) vtx = vx;
    }
  }
 
  for( auto pair: outputMuons ) { 

    ATH_MSG_DEBUG( "First Muon: " << pair.first );
    ATH_MSG_DEBUG( "Second Muon: " << pair.second );

    m_Lead_Pt = ( pair.first )->pt() / 1000.;
    m_Lead_Eta = ( pair.first )->eta();
    m_Lead_Phi = ( pair.first )->phi();
    m_Lead_Charge = ( pair.first )->charge(); 
    const xAOD::TrackParticle* Lead_primaryTrack = ( pair.first )->primaryTrackParticle();
    m_Lead_qOverP = Lead_primaryTrack->qOverP();
    m_Lead_qOverPerr = sqrt( Lead_primaryTrack->definingParametersCovMatrix()(4,4) );
    m_Lead_ReducedChi2 = Lead_primaryTrack->chiSquared() / Lead_primaryTrack->numberDoF();

    uint8_t value (0);
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPrecisionLayers) ) m_Lead_nPrecLayers = value;
    else m_Lead_nPrecLayers = -1;
    if ( pair.first->summaryValue(value, xAOD::numberOfGoodPrecisionLayers) ) m_Lead_nGoodPrecLayers = value;
    else m_Lead_nGoodPrecLayers = -1;
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPrecisionHoleLayers) ) m_Lead_nPrecHoles = value;
    else m_Lead_nPrecHoles = -1;
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPhiLayers) ) m_Lead_nPhiLayers = value;
    else m_Lead_nPhiLayers = -1;
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPhiHoleLayers) ) m_Lead_nPhiHoleLayers = value;
    else m_Lead_nPhiHoleLayers = -1;
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfTriggerEtaLayers) ) m_Lead_nTriggerEtaLayers = value;
    else m_Lead_nTriggerEtaLayers = -1;
    if ( Lead_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfTriggerEtaHoleLayers) ) m_Lead_nTriggerEtaHoleLayers = value;
    else m_Lead_nTriggerEtaHoleLayers = -1;

    //:::
    ATH_MSG_DEBUG( "Retrieving Leading ID Track..." );
    const xAOD::TrackParticle* Lead_idTrack = ( pair.first )->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    if (Lead_idTrack) {
      ATH_MSG_DEBUG( "Lead ID Track: " << Lead_idTrack );
      m_Lead_ID_Pt = Lead_idTrack->pt() / 1000.;
      m_Lead_ID_Eta = Lead_idTrack->eta();
      m_Lead_ID_Phi = Lead_idTrack->phi();
      m_Lead_ID_qOverP = Lead_idTrack->qOverP();
      m_Lead_ID_qOverPerr = sqrt( Lead_idTrack->definingParametersCovMatrix()(4,4) );
      m_Lead_d0_exBS = Lead_idTrack->d0();
      m_Lead_d0_exBSerr = xAOD::TrackingHelpers::d0significance( Lead_idTrack, evtInfo->beamPosSigmaX(), evtInfo->beamPosSigmaY(), evtInfo->beamPosSigmaXY() );
      m_Lead_z0_exBS = Lead_idTrack->z0();
      m_Lead_ID_ReducedChi2 = Lead_idTrack->chiSquared() / Lead_idTrack->numberDoF();
      if( vtx!=0 ) m_Lead_z0_exPV = Lead_idTrack->z0() + Lead_idTrack->vz() - vtx->z();
      else  m_Lead_z0_exPV = -9999999999.99;
    } else {
      m_Lead_ID_ReducedChi2 = -9999999999.99;
      m_Lead_z0_exPV = -9999999999.99;
      m_Lead_d0_exBS = -9999999999.99;
      m_Lead_d0_exBSerr = -9999999999.99;
      m_Lead_z0_exBS = -9999999999.99;
      m_Lead_ID_Pt = -9999999999.99;
      m_Lead_ID_Eta = -9999999999.99;	
      m_Lead_ID_Phi = -9999999999.99;
      m_Lead_ID_qOverP = -9999999999.99;
      m_Lead_ID_qOverPerr = -9999999999.99;
    }
    //:::
    ATH_MSG_DEBUG( "Retrieving Leading ME, MS Tracks..." );
    const xAOD::TrackParticle* Lead_meTrack;
    const xAOD::TrackParticle* Lead_msTrack;
    Lead_meTrack = ( pair.first )->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
    Lead_msTrack = ( pair.first )->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
    if ( Lead_meTrack  ) {
      ATH_MSG_DEBUG( "Lead ME Track: " << Lead_meTrack );
      m_Lead_ME_Pt =  Lead_meTrack->pt() / 1000.;
      m_Lead_ME_Eta = Lead_meTrack->eta();
      m_Lead_ME_Phi = Lead_meTrack->phi();
      m_Lead_ME_qOverP = Lead_meTrack->qOverP();
      m_Lead_ME_qOverPerr = sqrt( Lead_meTrack->definingParametersCovMatrix()(4,4) );
    } else {
      ATH_MSG_DEBUG( "Lead ME Track NOT Found...: " << std::endl );
      m_Lead_ME_Pt =  -9999999999.99;
      m_Lead_ME_Eta = -9999999999.99;
      m_Lead_ME_Phi = -9999999999.99;
      m_Lead_ME_qOverP = -9999999999.99;
      m_Lead_ME_qOverPerr = -9999999999.99;
    }    
    if ( Lead_msTrack  ) {
      ATH_MSG_DEBUG( "Lead MS Track: " << Lead_msTrack );
      m_Lead_MS_Pt =  Lead_msTrack->pt() / 1000.;
      m_Lead_MS_Eta = Lead_msTrack->eta();
      m_Lead_MS_Phi = Lead_msTrack->phi();
      m_Lead_MS_qOverP = Lead_msTrack->qOverP();
      m_Lead_MS_qOverPerr = sqrt( Lead_msTrack->definingParametersCovMatrix()(4,4) );
    } else {
      ATH_MSG_DEBUG( "Lead MS Track NOT Found...: " << std::endl );
      m_Lead_MS_Pt =  -9999999999.99;
      m_Lead_MS_Eta = -9999999999.99;
      m_Lead_MS_Phi = -9999999999.99;
      m_Lead_MS_qOverP = -9999999999.99;
      m_Lead_MS_qOverPerr = -9999999999.99;
    }

    m_Lead_Quality = ( pair.first )->auxdata< int >( "MST_Quality" );
    m_Lead_passedIDCuts = ( pair.first )->auxdata< bool >( "MST_passedIDCuts" );
    m_Lead_PtCone20 = ( pair.first )->isolation( xAOD::Iso::ptvarcone20 );
    m_Lead_PtCone30 = ( pair.first )->isolation( xAOD::Iso::ptvarcone30 );
    m_Lead_TopoEtCone20 = ( pair.first )->isolation( xAOD::Iso::topoetcone20 );
    m_Lead_TopoEtCone30 = ( pair.first )->isolation( xAOD::Iso::topoetcone30 );
    m_Lead_Author = ( pair.first )->auxdata< uint16_t >( "author" );

    m_Lead_Type = -1;
    if( ( pair.first )->muonType() == xAOD::Muon::MuonType::Combined )
      m_Lead_Type = 0;
    else if( ( pair.first )->muonType() == xAOD::Muon::MuonType::MuonStandAlone )
      m_Lead_Type = 1;
    else if( ( pair.first )->muonType() == xAOD::Muon::MuonType::SegmentTagged )
      m_Lead_Type = 2;
    else if( ( pair.first )->muonType() == xAOD::Muon::MuonType::CaloTagged )
      m_Lead_Type = 3;
    else if( ( pair.first )->muonType() == xAOD::Muon::MuonType::SiliconAssociatedForwardMuon )
      m_Lead_Type = 4;

    m_Lead_EnergyLoss = ( pair.first )->auxdata< float >( "EnergyLoss" );
    m_Lead_EnergyLoss_Meas = ( pair.first )->auxdata< float >( "MeasEnergyLoss" );
    m_Lead_EnergyLoss_Par = ( pair.first )->auxdata< float >( "ParamEnergyLoss" );
    m_Lead_EnergyLoss_Err = ( pair.first )->auxdata< float >( "EnergyLossSigma" ); 
    m_Lead_EnergyLossType = ( pair.first )->auxdata< uint8_t >( "energyLossType" );
    m_Lead_PrimarySector = ( pair.first )->auxdata< uint8_t >( "primarySector" );
    m_Lead_SecondarySector = ( pair.first )->auxdata< uint8_t >( "secondarySector" );

    // Corrected 
    m_Lead_Pt_Smeared = ( pair.first )->auxdata< float >( "CB_Pt_Smeared" ) /1000.;
    m_Lead_ID_Pt_Smeared = ( pair.first )->auxdata< float >( "ID_Pt_Smeared" ) /1000.;
    m_Lead_ME_Pt_Smeared = ( pair.first )->auxdata< float >( "ME_Pt_Smeared" ) /1000.;

    //::://:::
    m_Sub_Pt = ( pair.second )->pt() / 1000.;
    m_Sub_Eta = ( pair.second )->eta();
    m_Sub_Phi = ( pair.second )->phi();
    m_Sub_Charge = ( pair.second )->charge();  
    const xAOD::TrackParticle* Sub_primaryTrack = ( pair.second )->primaryTrackParticle();
    m_Sub_qOverP = Sub_primaryTrack->qOverP();
    m_Sub_qOverPerr = sqrt( Sub_primaryTrack->definingParametersCovMatrix()(4,4) );
    m_Sub_ReducedChi2 = Sub_primaryTrack->chiSquared() / Sub_primaryTrack->numberDoF();
    // ::

    value = 0;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPrecisionLayers) ) m_Sub_nPrecLayers = value;
    else m_Sub_nPrecLayers = -1;
    if ( pair.second->summaryValue(value, xAOD::numberOfGoodPrecisionLayers) ) m_Sub_nGoodPrecLayers = value;
    else m_Sub_nGoodPrecLayers = -1;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPrecisionHoleLayers) ) m_Sub_nPrecHoles = value;
    else m_Sub_nPrecHoles = -1;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPhiLayers) ) m_Sub_nPhiLayers = value;
    else m_Sub_nPhiLayers = -1;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfPhiHoleLayers) ) m_Sub_nPhiHoleLayers = value;
    else m_Sub_nPhiHoleLayers = -1;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfTriggerEtaLayers) ) m_Sub_nTriggerEtaLayers = value;
    else m_Sub_nTriggerEtaLayers = -1;
    if ( Sub_primaryTrack->summaryValue(value, xAOD::SummaryType::numberOfTriggerEtaHoleLayers) ) m_Sub_nTriggerEtaHoleLayers = value;
    else m_Sub_nTriggerEtaHoleLayers = -1;

    ATH_MSG_DEBUG( "Retrieving Subative ID Track..." );
    const xAOD::TrackParticle* Sub_idTrack = ( pair.second )->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    if ( Sub_idTrack ) {
      ATH_MSG_DEBUG( "Subative ID Track: " << Sub_idTrack );
      m_Sub_ID_Pt  = Sub_idTrack->pt() / 1000.;
      m_Sub_ID_Eta = Sub_idTrack->eta();
      m_Sub_ID_Phi = Sub_idTrack->phi();
      m_Sub_ID_qOverP = Sub_idTrack->qOverP();
      m_Sub_ID_qOverPerr = sqrt( Sub_idTrack->definingParametersCovMatrix()(4,4) );
      m_Sub_d0_exBS = Sub_idTrack->d0();
      m_Sub_d0_exBSerr = xAOD::TrackingHelpers::d0significance( Sub_idTrack, evtInfo->beamPosSigmaX(), evtInfo->beamPosSigmaY(), evtInfo->beamPosSigmaXY() );
      m_Sub_z0_exBS = Sub_idTrack->z0();
      m_Sub_ID_ReducedChi2 = Sub_idTrack->chiSquared() / Sub_idTrack->numberDoF();
      if( vtx!=0 ) m_Sub_z0_exPV = Sub_idTrack->z0() + Sub_idTrack->vz() - vtx->z();
      else  m_Sub_z0_exPV = -9999999999.99;
    } else {
      m_Sub_ID_ReducedChi2 = -9999999999.99;
      m_Sub_ID_Pt   = -9999999999.99;
      m_Sub_ID_Eta  = -9999999999.99;
      m_Sub_ID_Phi  = -9999999999.99;
      m_Sub_ID_qOverP  = -9999999999.99;
      m_Sub_ID_qOverPerr  = -9999999999.99;
      m_Sub_d0_exBS = -9999999999.99;
      m_Sub_d0_exBSerr = -9999999999.99;
      m_Sub_z0_exBS = -9999999999.99;
      m_Sub_z0_exPV = -9999999999.99;
    }
    //:::
    ATH_MSG_DEBUG( "Retrieving SubLeading ME, MS Tracks..." );
    const xAOD::TrackParticle* Sub_meTrack;
    const xAOD::TrackParticle* Sub_msTrack;
    Sub_meTrack = ( pair.second )->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
    Sub_msTrack = ( pair.second )->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
    if ( Sub_meTrack  ) {
      ATH_MSG_DEBUG( "Sub ME Track: " << Sub_meTrack );
      m_Sub_ME_Pt  = Sub_meTrack->pt() / 1000.;
      m_Sub_ME_Eta = Sub_meTrack->eta();
      m_Sub_ME_Phi = Sub_meTrack->phi();
      m_Sub_ME_qOverP = Sub_meTrack->qOverP();
      m_Sub_ME_qOverPerr = sqrt( Sub_meTrack->definingParametersCovMatrix()(4,4) );
    } else {
      ATH_MSG_DEBUG( "Sub ME Track NOT Found...: " << std::endl );
      m_Sub_ME_Pt  = -9999999999.99;
      m_Sub_ME_Eta = -9999999999.99;
      m_Sub_ME_Phi = -9999999999.99;
      m_Sub_ME_qOverP = -9999999999.99;
      m_Sub_ME_qOverPerr = -9999999999.99;
    }
    if ( Sub_msTrack  ) {
      ATH_MSG_DEBUG( "Sub MS Track: " << Sub_msTrack );
      m_Sub_MS_Pt  = Sub_msTrack->pt() / 1000.;
      m_Sub_MS_Eta = Sub_msTrack->eta();
      m_Sub_MS_Phi = Sub_msTrack->phi();
      m_Sub_MS_qOverP = Sub_msTrack->qOverP();
      m_Sub_MS_qOverPerr = sqrt( Sub_msTrack->definingParametersCovMatrix()(4,4) );
    } else {
      ATH_MSG_DEBUG( "Sub MS Track NOT Found...: " << std::endl );
      m_Sub_MS_Pt  = -9999999999.99;
      m_Sub_MS_Eta = -9999999999.99;
      m_Sub_MS_Phi = -9999999999.99;
      m_Sub_MS_qOverP = -9999999999.99;
      m_Sub_MS_qOverPerr = -9999999999.99;
    }
    
    m_Sub_Quality = ( pair.second )->auxdata< int >( "MST_Quality" );
    m_Sub_passedIDCuts = ( pair.second )->auxdata< bool >( "MST_passedIDCuts" );
    m_Sub_PtCone20 = ( pair.second )->isolation( xAOD::Iso::ptvarcone20 );
    m_Sub_PtCone30 = ( pair.second )->isolation( xAOD::Iso::ptvarcone30 );    
    m_Sub_TopoEtCone20 = ( pair.second )->isolation( xAOD::Iso::topoetcone20 );
    m_Sub_TopoEtCone30 = ( pair.second )->isolation( xAOD::Iso::topoetcone30 );
    m_Sub_Author = ( pair.second )->auxdata< uint16_t >( "author" );

    m_Sub_Type = -1;
    if( ( pair.second )->muonType() == xAOD::Muon::MuonType::Combined )
      m_Sub_Type = 0;
    else if( ( pair.second )->muonType() == xAOD::Muon::MuonType::MuonStandAlone )
      m_Sub_Type = 1;
    else if( ( pair.second )->muonType() == xAOD::Muon::MuonType::SegmentTagged )
      m_Sub_Type = 2;
    else if( ( pair.second )->muonType() == xAOD::Muon::MuonType::CaloTagged )
      m_Sub_Type = 3;
    else if( ( pair.second )->muonType() == xAOD::Muon::MuonType::SiliconAssociatedForwardMuon )
      m_Sub_Type = 4;

    m_Sub_EnergyLoss      = ( pair.second )->auxdata< float >( "EnergyLoss" );
    m_Sub_EnergyLoss_Meas = ( pair.second )->auxdata< float >( "MeasEnergyLoss" );
    m_Sub_EnergyLoss_Par = ( pair.second )->auxdata< float >( "ParamEnergyLoss" );
    m_Sub_EnergyLoss_Err = ( pair.second )->auxdata< float >( "EnergyLossSigma" ); 
    m_Sub_EnergyLossType  = ( pair.second )->auxdata< uint8_t >( "energyLossType" );
    m_Sub_PrimarySector   = ( pair.second )->auxdata< uint8_t >( "primarySector" );
    m_Sub_SecondarySector = ( pair.second )->auxdata< uint8_t >( "secondarySector" );

    // Corrected 
    m_Sub_Pt_Smeared = ( pair.second )->auxdata< float >( "CB_Pt_Smeared" ) /1000.;
    m_Sub_ID_Pt_Smeared = ( pair.second )->auxdata< float >( "ID_Pt_Smeared" ) /1000.;
    m_Sub_ME_Pt_Smeared = ( pair.second )->auxdata< float >( "ME_Pt_Smeared" ) /1000.;

    TLorentzVector Lead, Sub;

    Lead.SetPtEtaPhiM( m_Lead_Pt, m_Lead_Eta, m_Lead_Phi, MuonMass );
    Sub.SetPtEtaPhiM( m_Sub_Pt, m_Sub_Eta, m_Sub_Phi, MuonMass );
    m_CB_Mass = ( Lead + Sub ).M();
    m_CB_Pt   = ( Lead + Sub ).Pt(); 
    m_CB_Eta  = ( Lead + Sub ).Eta();
    m_CB_Phi  = ( Lead + Sub ).Phi();
    Sub.SetPtEtaPhiM( m_Sub_Pt_Smeared, m_Sub_Eta, m_Sub_Phi, MuonMass );
    Lead.SetPtEtaPhiM( m_Lead_Pt_Smeared, m_Lead_Eta, m_Lead_Phi, MuonMass );
    m_CB_Mass_Smeared = ( Lead + Sub ).M();

    Lead.SetPtEtaPhiM( m_Lead_ID_Pt, m_Lead_ID_Eta, m_Lead_ID_Phi, MuonMass );
    Sub.SetPtEtaPhiM( m_Sub_ID_Pt, m_Sub_ID_Eta, m_Sub_ID_Phi, MuonMass );
    m_ID_Mass = ( Lead + Sub ).M(); 
    m_ID_Pt   = ( Lead + Sub ).Pt();
    m_ID_Eta  = ( Lead + Sub ).Eta();
    m_ID_Phi  = ( Lead + Sub ).Phi();
    Sub.SetPtEtaPhiM( m_Sub_ID_Pt_Smeared, m_Sub_ID_Eta, m_Sub_ID_Phi, MuonMass );
    Lead.SetPtEtaPhiM( m_Lead_ID_Pt_Smeared, m_Lead_ID_Eta, m_Lead_ID_Phi, MuonMass );
    m_ID_Mass_Smeared = ( Lead + Sub ).M();

    Lead.SetPtEtaPhiM( m_Lead_ME_Pt, m_Lead_ME_Eta, m_Lead_ME_Phi, MuonMass );
    Sub.SetPtEtaPhiM( m_Sub_ME_Pt, m_Sub_ME_Eta, m_Sub_ME_Phi, MuonMass );
    m_ME_Mass = ( Lead + Sub ).M(); 
    m_ME_Pt   = ( Lead + Sub ).Pt();
    m_ME_Eta  = ( Lead + Sub ).Eta();
    m_ME_Phi  = ( Lead + Sub ).Phi();
    Sub.SetPtEtaPhiM( m_Sub_ME_Pt_Smeared, m_Sub_ME_Eta, m_Sub_ME_Phi, MuonMass );
    Lead.SetPtEtaPhiM( m_Lead_ME_Pt_Smeared, m_Lead_ME_Eta, m_Lead_ME_Phi, MuonMass );
    m_ME_Mass_Smeared = ( Lead + Sub ).M();

    m_OutputTree->Fill();

  }

  return StatusCode::SUCCESS;

}
