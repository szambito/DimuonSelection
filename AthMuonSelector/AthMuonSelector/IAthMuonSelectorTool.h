#ifndef __IAthMuonSelectorTool_h__
#define __IAthMuonSelectorTool_h__

//::: ROOT includes
#include "TH1F.h"

//::: Framework includes
#include "GaudiKernel/IAlgTool.h"

//::: EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

static const InterfaceID IID_IAthMuonSelectorTool( "IAthMuonSelectorTool", 1, 0 );

typedef std::vector< std::pair< const xAOD::Muon*, const xAOD::Muon* > > PairsVector;

class IAthMuonSelectorTool : virtual public ::IAlgTool { 

public: 

  virtual ~IAthMuonSelectorTool() {};

  static const InterfaceID& interfaceID();

  virtual PairsVector GetMuons( const xAOD::MuonContainer* the_muons, TH1F* cut_flow ) = 0;

protected: 

}; 

inline const InterfaceID& IAthMuonSelectorTool::interfaceID() { 

   return IID_IAthMuonSelectorTool; 

}


#endif 
