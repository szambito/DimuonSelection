#ifndef __AthMuonNtupleMaker_h__
#define __AthMuonNtupleMaker_h__

//::: STL includes
#include <string>

//::: ROOT includes
#include <TTree.h>
#include <TH1F.h>

//::: Framework includes
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" 
#include "GaudiKernel/ITHistSvc.h" 

//::: EDM includes
#include "xAODEventInfo/EventInfo.h"

//::: Local includes
#include "AthMuonSelector/IAthMuonSelectorTool.h"

#define MuonMass 0.10565837

class AthMuonNtupleMaker : public ::AthAlgorithm { 

public: 

  AthMuonNtupleMaker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~AthMuonNtupleMaker() {} 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

private: 

  bool                                  m_Extended;
  ToolHandle< IAthMuonSelectorTool > m_SelectorTool;
  ServiceHandle< ITHistSvc >            m_HistSvc;

  //:::
  TH1F* m_CutFlow;
  //:::
  TTree* m_OutputTree;
  //:::
  UInt_t m_RunNumber, m_LumiBlock, m_ChannelNumber, m_averageInteractionsPerCrossing;
  ULong64_t m_EvtNumber;
  Float_t m_EventWeight;
  //:::
  Float_t m_Lead_z0_exBS,          m_Sub_z0_exBS;
  Float_t m_Lead_z0_exPV,          m_Sub_z0_exPV;
  Float_t m_Lead_d0_exBS,          m_Sub_d0_exBS;
  Float_t m_Lead_d0_exBSerr,       m_Sub_d0_exBSerr;
  Float_t m_Lead_Pt,               m_Sub_Pt;
  Float_t m_Lead_Eta,              m_Sub_Eta;
  Float_t m_Lead_Phi,              m_Sub_Phi;
  Float_t m_Lead_qOverP,           m_Sub_qOverP;
  Float_t m_Lead_qOverPerr,        m_Sub_qOverPerr;
  Float_t m_Lead_Charge,           m_Sub_Charge;
  Float_t m_Lead_ID_Pt,            m_Sub_ID_Pt;
  Float_t m_Lead_ID_Eta,           m_Sub_ID_Eta;
  Float_t m_Lead_ID_Phi,           m_Sub_ID_Phi;
  Float_t m_Lead_ID_qOverP,        m_Sub_ID_qOverP;
  Float_t m_Lead_ID_qOverPerr,     m_Sub_ID_qOverPerr;
  Float_t m_Lead_ME_Pt,            m_Sub_ME_Pt;
  Float_t m_Lead_ME_Eta,           m_Sub_ME_Eta;
  Float_t m_Lead_ME_Phi,           m_Sub_ME_Phi;
  Float_t m_Lead_ME_qOverP,        m_Sub_ME_qOverP;
  Float_t m_Lead_ME_qOverPerr,     m_Sub_ME_qOverPerr;
  Float_t m_Lead_MS_Pt,            m_Sub_MS_Pt;
  Float_t m_Lead_MS_Eta,           m_Sub_MS_Eta;
  Float_t m_Lead_MS_Phi,           m_Sub_MS_Phi;
  Float_t m_Lead_MS_qOverP,        m_Sub_MS_qOverP;
  Float_t m_Lead_MS_qOverPerr,     m_Sub_MS_qOverPerr;
  Float_t m_Lead_PtCone20,         m_Sub_PtCone20;
  Float_t m_Lead_PtCone30,         m_Sub_PtCone30;
  Float_t m_Lead_TopoEtCone20,     m_Sub_TopoEtCone20;
  Float_t m_Lead_TopoEtCone30,     m_Sub_TopoEtCone30;
  Int_t   m_Lead_Quality,          m_Sub_Quality;
  Int_t   m_Lead_Author,           m_Sub_Author;
  Int_t   m_Lead_Type,             m_Sub_Type;
  Float_t m_Lead_EnergyLoss,       m_Sub_EnergyLoss;
  Float_t m_Lead_EnergyLoss_Err,   m_Sub_EnergyLoss_Err;
  Float_t m_Lead_EnergyLoss_Meas,  m_Sub_EnergyLoss_Meas;
  Float_t m_Lead_EnergyLoss_Par,   m_Sub_EnergyLoss_Par;
  Int_t   m_Lead_EnergyLossType,   m_Sub_EnergyLossType;
  Int_t   m_Lead_PrimarySector,    m_Sub_PrimarySector;
  Int_t   m_Lead_SecondarySector,  m_Sub_SecondarySector;
  Float_t m_CB_Mass;
  Float_t m_CB_Mass_Smeared;
  Float_t m_ID_Mass;
  Float_t m_ID_Mass_Smeared;
  Float_t m_ME_Mass;
  Float_t m_ME_Mass_Smeared;
  Float_t m_CB_Pt;
  Float_t m_ID_Pt;
  Float_t m_ME_Pt;
  Float_t m_CB_Eta;
  Float_t m_ID_Eta;
  Float_t m_ME_Eta;
  Float_t m_CB_Phi;
  Float_t m_ID_Phi;
  Float_t m_ME_Phi;
  Float_t m_Lead_Pt_Smeared, m_Sub_Pt_Smeared;
  Float_t m_Lead_ID_Pt_Smeared, m_Sub_ID_Pt_Smeared;
  Float_t m_Lead_ME_Pt_Smeared, m_Sub_ME_Pt_Smeared;
  Int_t   m_Lead_nPrecLayers , m_Sub_nPrecLayers,
    m_Lead_nGoodPrecLayers , m_Sub_nGoodPrecLayers,
    m_Lead_nPrecHoles , m_Sub_nPrecHoles,
    m_Lead_nPhiLayers , m_Sub_nPhiLayers,
    m_Lead_nPhiHoleLayers , m_Sub_nPhiHoleLayers,
    m_Lead_nTriggerEtaLayers , m_Sub_nTriggerEtaLayers,
    m_Lead_nTriggerEtaHoleLayers , m_Sub_nTriggerEtaHoleLayers;
  Float_t m_Lead_ReducedChi2, m_Sub_ReducedChi2, m_Lead_ID_ReducedChi2, m_Sub_ID_ReducedChi2;
  Bool_t  m_Lead_passedIDCuts, m_Sub_passedIDCuts;
  //:::

  AthMuonNtupleMaker();

}; 

#endif
