# echo "cleanup AthMuonSelector AthMuonSelector-00-00-01 in /afs/cern.ch/user/s/szambito/public/DimuonSelection"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtAthMuonSelectortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtAthMuonSelectortempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=AthMuonSelector -version=AthMuonSelector-00-00-01 -path=/afs/cern.ch/user/s/szambito/public/DimuonSelection  $* >${cmtAthMuonSelectortempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=AthMuonSelector -version=AthMuonSelector-00-00-01 -path=/afs/cern.ch/user/s/szambito/public/DimuonSelection  $* >${cmtAthMuonSelectortempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtAthMuonSelectortempfile}
  unset cmtAthMuonSelectortempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtAthMuonSelectortempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtAthMuonSelectortempfile}
unset cmtAthMuonSelectortempfile
return $cmtcleanupstatus

