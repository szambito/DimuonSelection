# echo "setup AthMuonSelector AthMuonSelector-00-00-01 in /afs/cern.ch/user/s/szambito/public/DimuonSelection"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtAthMuonSelectortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtAthMuonSelectortempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=AthMuonSelector -version=AthMuonSelector-00-00-01 -path=/afs/cern.ch/user/s/szambito/public/DimuonSelection  -no_cleanup $* >${cmtAthMuonSelectortempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=AthMuonSelector -version=AthMuonSelector-00-00-01 -path=/afs/cern.ch/user/s/szambito/public/DimuonSelection  -no_cleanup $* >${cmtAthMuonSelectortempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtAthMuonSelectortempfile}
  unset cmtAthMuonSelectortempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtAthMuonSelectortempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtAthMuonSelectortempfile}
unset cmtAthMuonSelectortempfile
return $cmtsetupstatus

